import React, { Component } from "react";

export default class Header extends Component {
  render() {
    return (
      <header>
        <div className="container">
          <div className="branding-logo">
            <h1>Simple Weather App</h1>
          </div>
        </div>
      </header>
    );
  }
}
