import React, { Component } from "react";

export default class Box extends Component {
  render() {
    return (
      <div className="box">
        <div className="location">
          <p>{this.props.day}</p>
        </div>
        <div className="temperature">
          <h4>{this.props.maxTemp} &#8451;</h4>
          <p>{this.props.minTemp} &#8451;</p>
        </div>
        <div className="weather-icon">
          <img
            src={
              "http://openweathermap.org/img/wn/" + this.props.icon + "@2x.png"
            }
            alt="Weather"
          />
        </div>
        <div className="weather-description">{this.props.description}</div>
      </div>
    );
  }
}
