import React, { Component } from "react";

export default class CurrentWeather extends Component {
  render() {
    return (
      <div className="container" style={{ marginTop: "1rem" }}>
        <div className="current-weather">
          <h1 className="search-city-name">{this.props.city}</h1>
          <div className="current-weather-box">
            <div className="weather-icon">
              <img
                src={
                  "http://openweathermap.org/img/wn/" +
                  this.props.icon +
                  "@2x.png"
                }
                alt="Weather"
              />
              <p>Feels like {this.props.feelsLike} &#8451;</p>
            </div>
            <div className="temperature">
              <div className="temperature">
                <h1>{this.props.temperature} &#8451;</h1>
              </div>

              <div className="weather-description">
                {this.props.weatherDescription}
              </div>
            </div>

            <div className="description">
              <div className="weather-description">
                <h3>{this.props.weatherDescription}</h3>
                <p>
                  Humidity &nbsp;<strong>{this.props.humidity}%</strong>
                </p>
                <p>
                  Wind &nbsp;<strong>{this.props.wind} km/h</strong>
                </p>
                <p>
                  Min &nbsp;<strong>{this.props.minTemperature} &#8451;</strong>
                </p>
                <p>
                  Max &nbsp;<strong>{this.props.maxTemperature} &#8451;</strong>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
