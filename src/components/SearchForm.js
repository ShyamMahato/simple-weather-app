import React, { Component } from "react";

import axiosInstance from "../axiosInstance";

import CurrentWeather from "./CurrentWeather";
import ForecastWeather from "./ForecastWeather";

export default class SearchForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      city: "Bengaluru",

      temperature: "",
      cityName: "Bengaluru",
      minTemperature: "",
      maxTemperature: "",
      weatherDescription: "",
      icon: "",
      feelsLike: "",
      humidity: "",
      wind: "",

      cityLat: "",
      cityLong: "",

      cityError: "",
      submitButtonLoading: false,

      foreCastData: [],
    };
  }

  isNumeric = (str) => {
    if (typeof str != "string") return false;
    return !isNaN(str) && !isNaN(parseFloat(str));
  };

  fetchData = async () => {
    try {
      const currentWeatherData = await axiosInstance.get(
        `/weather?appid=686ed40289dcbef320cc9344da200ba2&q=${this.state.city}&units=metric`
      );

      this.setState(
        {
          cityLong: currentWeatherData.data.coord.lon,
          cityLat: currentWeatherData.data.coord.lat,
          cityName: currentWeatherData.data.name,
        },
        async () => {
          const forecastWeatherData = await axiosInstance.get(
            `/onecall?lat=${this.state.cityLat}&lon=${this.state.cityLong}&appid=686ed40289dcbef320cc9344da200ba2&units=metric&exclude=current,hourly,minutely`
          );

          this.setState({
            cityError: "",
            submitButtonLoading: false,

            foreCastData: forecastWeatherData.data.daily,
            temperature: Math.round(currentWeatherData.data.main.temp),
            minTemperature: Math.round(currentWeatherData.data.main.temp_min),
            maxTemperature: Math.round(currentWeatherData.data.main.temp_max),
            feelsLike: Math.round(currentWeatherData.data.main.feels_like),
            humidity: currentWeatherData.data.main.humidity,
            weatherDescription: currentWeatherData.data.weather[0].main,
            icon: currentWeatherData.data.weather[0].icon,
            wind: Math.round(currentWeatherData.data.wind.speed),
          });
        }
      );
    } catch (error) {
      this.setState({
        submitButtonLoading: false,
        cityError: error.response.data.message,
      });
    }
  };

  submitCity = (event) => {
    event.preventDefault();
    this.setState(
      {
        submitButtonLoading: true,
      },
      () => {
        this.fetchData();
      }
    );
  };

  handleChange = (event) => {
    if (this.isNumeric(event.target.value)) {
      this.setState({
        cityError: "City is not valid",
      });
    } else {
      this.setState({
        formSubmit: false,
        cityError: "",
        city: event.target.value,
      });
    }
  };

  componentDidMount() {
    this.fetchData();
  }

  render() {
    return (
      <>
        <div className="container">
          <form onSubmit={this.submitCity}>
            <div className="search-box">
              <div className="form-fields">
                <div className="input-box">
                  <input
                    type="text"
                    placeholder="Enter city name"
                    name="city"
                    defaultValue={this.state.city}
                    onChange={this.handleChange}
                    className={
                      this.state.cityError.length === 0
                        ? "form-control"
                        : "form-control error"
                    }
                  />
                  {this.state.cityError.length ? (
                    <span className="input-field-error">
                      {this.state.cityError}
                    </span>
                  ) : (
                    ""
                  )}
                </div>
              </div>
              <div className="form-fields">
                <div className="button-box">
                  {this.state.submitButtonLoading ? (
                    <button
                      type="button"
                      disabled
                      style={{ cursor: "not-allowed" }}
                    >
                      Please wait...
                    </button>
                  ) : (
                    <button type="submit">Submit</button>
                  )}
                </div>
              </div>
            </div>
          </form>
        </div>

        <CurrentWeather
          city={this.state.cityName}
          icon={this.state.icon}
          temperature={this.state.temperature}
          weatherDescription={this.state.weatherDescription}
          minTemperature={this.state.minTemperature}
          maxTemperature={this.state.maxTemperature}
          feelsLike={this.state.feelsLike}
          humidity={this.state.humidity}
          wind={this.state.wind}
        />

        <ForecastWeather foreCastData={this.state.foreCastData} />
      </>
    );
  }
}
