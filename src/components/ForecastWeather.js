import React, { Component } from "react";

import Box from "./Box";

const days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];

export default class ForecastWeather extends Component {
  render() {
    return (
      <>
        <div
          className="container forecast-heading"
          style={{ marginTop: "3rem" }}
        >
          <h1>Next 5 days Forecast</h1>
        </div>
        <div
          className="container mobile-container"
          style={{ marginTop: "1rem" }}
        >
          {this.props.foreCastData
            ? this.props.foreCastData.slice(1, 6).map((dateObj, index) => {
                const date = new Date(dateObj.dt * 1000);
                const dayName = days[date.getDay()];
                const day = dayName + ", " + date.getDate();
                return (
                  <Box
                    key={index}
                    day={day}
                    minTemp={Math.round(dateObj.temp.min)}
                    maxTemp={Math.round(dateObj.temp.max)}
                    icon={dateObj.weather[0].icon}
                    description={dateObj.weather[0].main}
                  />
                );
              })
            : ""}
        </div>
      </>
    );
  }
}
