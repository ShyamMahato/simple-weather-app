import "./App.css";

import Header from "./components/Header";
import SearchForm from "./components/SearchForm";

function App() {
  return (
    <>
      <Header />
      <SearchForm />
    </>
  );
}

export default App;
